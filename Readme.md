# CSharp Universal Tween Engine

 This is a port of [Java Universal Tween Engine](http://code.google.com/p/java-universal-tween-engine/). 
 
## Introduction

  The Universal Tween Engine enables the interpolation of every attribute from any object in any C# project. Implement the TweenAccessor interface, register it to the engine, and animate anything you want! 
  In one line, send your objects to another position (here x=20 and y=30), with a smooth elastic transition, during 1 second).
  
  Main features are: 
	- Supports every interpolation function defined by Robert Penner: http://www.robertpenner.com/easing/ 
	- Can be used with any object. You just have to implement the TweenAccessor interface when you want interpolation capacities. 
	- Every attribute can be interpolated. The only requirement is that what you want to interpolate can be represented as a float number. 
	- One line is sufficient to create and start a simple interpolation. 
	- Delays can be specified, to trigger the interpolation only after some time. 
	- Many callbacks can be specified (when tweens complete, start, end, etc.). 
	- Tweens and Timelines are pooled by default. If enabled, there won't be any object allocation during runtime! You can safely use it in Android game development without fearing the garbage collector. 
	- Tweens can be sequenced when used in Timelines. 
	- Tweens can act on more than one value at a time, so a single tween can change the whole position (X and Y) of a sprite for instance ! 
	- Tweens and Timelines can be repeated, with a yoyo style option. 
	- Simple timers can be built with Tween.Call().
 
## Changelog: 
 
 + 1.0.0
   - Initial version (based on 6.3.3 version)
 