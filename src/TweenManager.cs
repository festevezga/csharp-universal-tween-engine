﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AurelienRibon.TweenEngine
{
    public class TweenManager
    {
        private readonly IList<BaseTween> objects = new List<BaseTween>(20);

        private bool isPaused = false;

        public static void SetAutoRemove(BaseTween @object, bool value)
        {
            @object.IsAutoRemoveEnabled = value;
        }

        public static void SetAutoStart(BaseTween @object, bool value)
        {
            @object.IsAutoStartEnabled = value;
        }

        private static int GetTweensCount(IList<BaseTween> objs)
        {
            int cnt = 0;
            for(int i = 0, n = objs.Count; i < n; i++)
            {
                BaseTween obj = objs[i];
                if(obj is Tween)
                {
                    cnt += 1;
                }
                else
                {
                    cnt += GetTweensCount(((Timeline)obj).GetChildren());
                }
            }
            return cnt;
        }

        private static int GetTimelinesCount(IList<BaseTween> objs)
        {
            int cnt = 0;
            for(int i = 0, n = objs.Count; i < n; i++)
            {
                BaseTween obj = objs[i];
                if(obj is Timeline)
                {
                    cnt += 1 + GetTimelinesCount(((Timeline)obj).GetChildren());
                }
            }
            return cnt;
        }

        public TweenManager Add(BaseTween @object)
        {
            if(!objects.Contains(@object))
            {
                objects.Add(@object);
            }

            if(@object.IsAutoStartEnabled)
            {
                @object.Start();
            }
            return this;
        }

        public bool ContainsTarget(object target)
        {
            for(int i = 0, n = objects.Count; i < n; i++)
            {
                BaseTween obj = objects[i];
                if(obj.ContainsTarget(target))
                {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsTarget(object target, int tweenType)
        {
            for(int i = 0, n = objects.Count; i < n; i++)
            {
                BaseTween obj = objects[i];
                if(obj.ContainsTarget(target, tweenType))
                {
                    return true;
                }
            }
            return false;
        }

        public void KillAll()
        {
            for(int i = 0, n = objects.Count; i < n; i++)
            {
                BaseTween obj = objects[i];
                obj.Kill();
            }
        }

        public void KillTarget(object target)
        {
            for(int i = 0, n = objects.Count; i < n; i++)
            {
                BaseTween obj = objects[i];
                obj.KillTarget(target);
            }
        }

        public void KillTarget(object target, int tweenType)
        {
            for(int i = 0, n = objects.Count; i < n; i++)
            {
                BaseTween obj = objects[i];
                obj.KillTarget(target, tweenType);
            }
        }

        public void Pause()
        {
            isPaused = true;
        }

        public void Resume()
        {
            isPaused = false;
        }

        public void Update(float delta)
        {
            for(int i = objects.Count - 1; i >= 0; i--)
            {
                BaseTween obj = objects[i];
                if(obj.IsFinished && obj.IsAutoRemoveEnabled)
                {
                    objects.RemoveAt(i);
                    obj.Free();
                }
            }

            if(!isPaused)
            {
                if(delta >= 0)
                {
                    for(int i = 0, n = objects.Count; i < n; i++)
                    {
                        objects[i].Update(delta);
                    }
                }
                else
                {
                    for(int i = objects.Count - 1; i >= 0; i--)
                    {
                        objects[i].Update(delta);
                    }
                }
            }
        }

        public ReadOnlyCollection<BaseTween> Objects
        {
            get
            {
                return ((List<BaseTween>)objects).AsReadOnly();
            }
        }

        public int RunningTimelinesCount
        {
            get
            {
                return GetTimelinesCount(objects);
            }
        }

        public int RunningTweensCount
        {
            get
            {
                return GetTweensCount(objects);
            }
        }

        public int Size
        {
            get
            {
                return objects.Count;
            }
        }
    }
}
