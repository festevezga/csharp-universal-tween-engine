using System;
using System.Collections.Generic;
using AurelienRibon.TweenEngine.Equations;

namespace AurelienRibon.TweenEngine
{
    public sealed class Tween : BaseTween
    {
        public const int Infinity = -1;

        private class PoolCallback : Pool<Tween>.ICallback
        {
            public void onPool(Tween obj)
            {
                obj.Reset();
            }
            public void onUnPool(Tween obj)
            {
                obj.Reset();
            }
        }

        private class PoolTween : Pool<Tween>
        {
            public PoolTween(int initCapacity, ICallback callback)
                : base(initCapacity, callback)
            {

            }

            protected override Tween Create()
            {
                return new Tween();
            }
        }

        private static readonly PoolCallback _poolCallback = new PoolCallback();

        private static readonly PoolTween _pool = new PoolTween(20, _poolCallback);

        private static readonly IDictionary<Type, TweenAccessor> _registeredAccessors = new Dictionary<Type, TweenAccessor>();

        private static int combinedAttrsLimit = 3;
        private static int waypointsLimit = 0;

        private Tween()
        {
            AccessorBuffer = new float[combinedAttrsLimit];
            PathBuffer = new float[(2 + waypointsLimit) * combinedAttrsLimit];
            StartValues = new float[combinedAttrsLimit];
            TargetValues = new float[combinedAttrsLimit];
            Waypoints = new float[waypointsLimit * combinedAttrsLimit];
            Reset();
        }

        public static void SetCombinedAttributesLimit(int limit)
        {
            Tween.combinedAttrsLimit = limit;
        }

        public static void SetWaypointsLimit(int limit)
        {
            Tween.waypointsLimit = limit;
        }

        public static Tween Call(TweenCallback callback)
        {
            Tween tween = _pool.Get();
            tween.Setup(null, -1, 0);
            tween.SetCallback(callback);
            tween.SetCallbackTriggers(TweenCallback.Start);
            return tween;
        }

        public static Tween From(object target, int tweenType, float duration)
        {
            Tween tween = _pool.Get();
            tween.Setup(target, tweenType, duration);
            tween.Ease(Quad.INOUT);
            tween.Path(TweenPaths.CatmullRom);
            tween.IsFrom = true;
            return tween;
        }

        public static TweenAccessor GetRegisteredAccessor(Type someClass)
        {
            return _registeredAccessors[someClass];
        }

        public static Tween Mark()
        {
            Tween tween = _pool.Get();
            tween.Setup(null, -1, 0);
            return tween;
        }

        public static void RegisterAccessor(Type someClass, TweenAccessor defaultAccessor)
        {
            _registeredAccessors.Add(someClass, defaultAccessor);
        }

        public static Tween Set(object target, int tweenType)
        {
            Tween tween = _pool.Get();
            tween.Setup(target, tweenType, 0);
            tween.Ease(Quad.INOUT);
            return tween;
        }

        public static Tween To(object target, int tweenType, float duration)
        {
            Tween tween = _pool.Get();
            tween.Setup(target, tweenType, duration);
            tween.Ease(Quad.INOUT);
            tween.Path(TweenPaths.CatmullRom);
            return tween;
        }

        public override BaseTween Build()
        {
            if(TargetObject == null)
            {
                return this;
            }

            Accessor = (TweenAccessor)_registeredAccessors[TargetClass];
            if(Accessor == null && TargetObject is TweenAccessor)
            {
                Accessor = (TweenAccessor)TargetObject;
            }

            if(Accessor != null)
            {
                CombinedAttributesCount = Accessor.GetValues(TargetObject, Type, AccessorBuffer);
            }
            else
            {
                throw new TweenException("No TweenAccessor was found for the target");
            }

            if(CombinedAttributesCount > combinedAttrsLimit)
            {
                ThrowCombinedAttrsLimitReached();
            }

            return this;
        }

        public Tween Cast(Type targetClass)
        {
            if(IsStarted)
            {
                throw new TweenException("You can't cast the target of a tween once it is started");
            }
            TargetClass = targetClass;
            return this;
        }
        
        public Tween Ease(TweenEquation easeEquation)
        {
            Easing = easeEquation;
            return this;
        }

        public override void Free()
        {
            _pool.Free(this);
        }

        public Tween Path(TweenPath path)
        {
            TweenPath = path;
            return this;
        }

        public Tween Target(float targetValue)
        {
            TargetValues[0] = targetValue;
            return this;
        }

        public Tween Target(float targetValue1, float targetValue2)
        {
            TargetValues[0] = targetValue1;
            TargetValues[1] = targetValue2;
            return this;
        }

        public Tween Target(float targetValue1, float targetValue2, float targetValue3)
        {
            TargetValues[0] = targetValue1;
            TargetValues[1] = targetValue2;
            TargetValues[2] = targetValue3;
            return this;
        }

        public Tween Target(params float[] targetValues)
        {
            if(targetValues.Length > combinedAttrsLimit)
            {
                ThrowCombinedAttrsLimitReached();
            }

            Array.Copy(targetValues, 0, TargetValues, 0, targetValues.Length);
            return this;
        }

        public Tween TargetRelative(float targetValue)
        {
            IsRelative = true;
            TargetValues[0] = IsInitialized ? targetValue + StartValues[0] : targetValue;
            return this;
        }

        public Tween TargetRelative(float targetValue1, float targetValue2)
        {
            IsRelative = true;
            TargetValues[0] = IsInitialized ? targetValue1 + StartValues[0] : targetValue1;
            TargetValues[1] = IsInitialized ? targetValue2 + StartValues[1] : targetValue2;
            return this;
        }

        public Tween TargetRelative(float targetValue1, float targetValue2, float targetValue3)
        {
            IsRelative = true;
            TargetValues[0] = IsInitialized ? targetValue1 + StartValues[0] : targetValue1;
            TargetValues[1] = IsInitialized ? targetValue2 + StartValues[1] : targetValue2;
            TargetValues[2] = IsInitialized ? targetValue3 + StartValues[2] : targetValue3;
            return this;
        }

        public Tween TargetRelative(params float[] targetValues)
        {
            if(targetValues.Length > combinedAttrsLimit)
            {
                ThrowCombinedAttrsLimitReached();
            }

            for(int i = 0; i < targetValues.Length; i++)
            {
                TargetValues[i] = IsInitialized ? targetValues[i] + StartValues[i] : targetValues[i];
            }

            IsRelative = true;
            return this;
        }

        public Tween Waypoint(float targetValue)
        {
            if(WaypointsCnt == waypointsLimit)
            {
                ThrowWaypointsLimitReached();
            }

            Waypoints[WaypointsCnt] = targetValue;
            WaypointsCnt += 1;
            return this;
        }

        public Tween Waypoint(float targetValue1, float targetValue2)
        {
            if(WaypointsCnt == waypointsLimit)
            {
                ThrowWaypointsLimitReached();
            }

            Waypoints[WaypointsCnt * 2] = targetValue1;
            Waypoints[WaypointsCnt * 2 + 1] = targetValue2;
            WaypointsCnt += 1;
            return this;
        }

        public Tween Waypoint(float targetValue1, float targetValue2, float targetValue3)
        {
            if(WaypointsCnt == waypointsLimit)
            {
                ThrowWaypointsLimitReached();
            }

            Waypoints[WaypointsCnt * 3] = targetValue1;
            Waypoints[WaypointsCnt * 3 + 1] = targetValue2;
            Waypoints[WaypointsCnt * 3 + 2] = targetValue3;
            WaypointsCnt += 1;
            return this;
        }

        public Tween Waypoint(params float[] targetValues)
        {
            if(WaypointsCnt == waypointsLimit)
            {
                ThrowWaypointsLimitReached();
            }

            Array.Copy(targetValues, 0, Waypoints, WaypointsCnt * targetValues.Length, targetValues.Length);
            WaypointsCnt += 1;
            return this;
        }

        protected override void InitializeOverride()
        {
            if(TargetObject == null)
            {
                return;
            }

            Accessor.GetValues(TargetObject, Type, StartValues);

            for(int i = 0; i < CombinedAttributesCount; i++)
            {
                TargetValues[i] += IsRelative ? StartValues[i] : 0;

                for(int ii = 0; ii < WaypointsCnt; ii++)
                {
                    Waypoints[ii * CombinedAttributesCount + i] += IsRelative ? StartValues[i] : 0;
                }

                if(IsFrom)
                {
                    float tmp = StartValues[i];
                    StartValues[i] = TargetValues[i];
                    TargetValues[i] = tmp;
                }
            }
        }

        protected override void Reset()
        {
            base.Reset();

            TargetObject = null;
            TargetClass = null;
            Accessor = null;
            Type = -1;
            Easing = null;
            TweenPath = null;

            IsFrom = IsRelative = false;
            CombinedAttributesCount = WaypointsCnt = 0;

            if(AccessorBuffer.Length != combinedAttrsLimit)
            {
                AccessorBuffer = new float[combinedAttrsLimit];
            }

            if(PathBuffer.Length != (2 + waypointsLimit) * combinedAttrsLimit)
            {
                PathBuffer = new float[(2 + waypointsLimit) * combinedAttrsLimit];
            }
        }

        protected override void UpdateOverride(int step, int lastStep, bool isIterationStep, float delta)
        {
            if(TargetObject == null || Easing == null)
            {
                return;
            }

            if(!isIterationStep && step > lastStep)
            {
                Accessor.SetValues(TargetObject, Type, IsReverse(lastStep) ? StartValues : TargetValues);
                return;
            }

            if(!isIterationStep && step < lastStep)
            {
                Accessor.SetValues(TargetObject, Type, IsReverse(lastStep) ? TargetValues : StartValues);
                return;
            }

            if(Duration < 0.00000000001f && delta > -0.00000000001f)
            {
                Accessor.SetValues(TargetObject, Type, IsReverse(step) ? TargetValues : StartValues);
                return;
            }

            if(Duration < 0.00000000001f && delta < 0.00000000001f)
            {
                Accessor.SetValues(TargetObject, Type, IsReverse(step) ? StartValues : TargetValues);
                return;
            }

            float time = IsReverse(step) ? Duration - CurrentTime : CurrentTime;
            float t = Easing.Compute(time / Duration);

            if(WaypointsCnt == 0 || TweenPath == null)
            {
                for(int i = 0; i < CombinedAttributesCount; i++)
                {
                    AccessorBuffer[i] = StartValues[i] + t * (TargetValues[i] - StartValues[i]);
                }

            }
            else
            {
                for(int i = 0; i < CombinedAttributesCount; i++)
                {
                    PathBuffer[0] = StartValues[i];
                    PathBuffer[1 + WaypointsCnt] = TargetValues[i];
                    for(int ii = 0; ii < WaypointsCnt; ii++)
                    {
                        PathBuffer[ii + 1] = Waypoints[ii * CombinedAttributesCount + i];
                    }

                    AccessorBuffer[i] = TweenPath.Compute(t, PathBuffer, WaypointsCnt + 2);
                }
            }

            Accessor.SetValues(TargetObject, Type, AccessorBuffer);
        }

        private Type FindTargetClass()
        {
            if(_registeredAccessors.ContainsKey(TargetObject.GetType()))
            {
                return TargetObject.GetType();
            }

            if(TargetObject is TweenAccessor)
            {
                return TargetObject.GetType();
            }

            Type parentClass = TargetObject.GetType().BaseType;
            while(parentClass != null && !_registeredAccessors.ContainsKey(parentClass))
            {
                parentClass = parentClass.BaseType;
            }

            return parentClass;
        }

        private void Setup(object target, int tweenType, float duration)
        {
            if(duration < 0)
            {
                throw new TweenException("Duration can't be negative");
            }

            TargetObject = target;
            TargetClass = target != null ? FindTargetClass() : null;
            Type = tweenType;
            Duration = duration;
        }

        private void ThrowCombinedAttrsLimitReached()
        {
            string msg = "You cannot combine more than " + combinedAttrsLimit + " "
                + "attributes in a tween. You can raise this limit with "
                    + "Tween.setCombinedAttributesLimit(), which should be called once "
                    + "in application initialization code.";
            throw new TweenException(msg);
        }

        private void ThrowWaypointsLimitReached()
        {
            string msg = "You cannot add more than " + waypointsLimit + " "
                + "waypoints to a tween. You can raise this limit with "
                    + "Tween.setWaypointsLimit(), which should be called once in "
                    + "application initialization code.";
            throw new TweenException(msg);
        }

        internal override bool ContainsTarget(object target)
        {
            return TargetObject == target;
        }

        internal override bool ContainsTarget(object target, int tweenType)
        {
            return TargetObject == target && Type == tweenType;
        }

        internal override void ForceEndValues()
        {
            if(TargetObject == null)
            {
                return;
            }

            Accessor.SetValues(TargetObject, Type, TargetValues);
        }

        internal override void ForceStartValues()
        {
            if(TargetObject == null)
            {
                return;
            }

            Accessor.SetValues(TargetObject, Type, StartValues);
        }

        public static int PoolSize
        {
            get
            {
                return _pool.Size;
            }
        }

        public static string Version
        {
            get
            {
                return "1.0.0";
            }
        }

        public TweenAccessor Accessor
        {
            get;
            internal set;
        }

        public int CombinedAttributesCount
        {
            get;
            internal set;
        }

        public TweenEquation Easing
        {
            get;
            internal set;
        }

        public Type TargetClass
        {
            get;
            internal set;
        }

        public object TargetObject
        {
            get;
            internal set;
        }

        public int Type
        {
            get;
            internal set;
        }

        private float[] AccessorBuffer
        {
            get;
            set;
        }

        private bool IsFrom
        {
            get;
            set;
        }

        private bool IsRelative
        {
            get;
            set;
        }

        private float[] PathBuffer
        {
            get;
            set;
        }

        private float[] StartValues
        {
            get;
            set;
        }

        private float[] TargetValues
        {
            get;
            set;
        }

        private TweenPath TweenPath
        {
            get;
            set;
        }

        private float[] Waypoints
        {
            get;
            set;
        }

        private int WaypointsCnt
        {
            get;
            set;
        }
    }
}