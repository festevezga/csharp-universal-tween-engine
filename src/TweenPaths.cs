﻿using AurelienRibon.TweenEngine.Paths;

namespace AurelienRibon.TweenEngine
{
    public sealed class TweenPaths 
    {
	    public static readonly Linear Linear = new Linear();
        public static readonly CatmullRom CatmullRom = new CatmullRom();
    }
}
