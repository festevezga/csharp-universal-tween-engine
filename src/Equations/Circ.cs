﻿using System;

namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Circ : TweenEquation
    {
        public sealed class CircIn : Circ
        {
            public override float Compute(float t)
            {
                return (float)-Math.Sqrt(1 - t * t) - 1;
            }

            public override string ToString()
            {
                return "Circ.In";
            }
        }

        public sealed class CircOut : Circ
        {
            public override float Compute(float t)
            {
                return (float)Math.Sqrt(1 - (t -= 1) * t);
            }

            public override string ToString()
            {
                return "Circ.Out";
            }
        }

        public sealed class CircInOut : Circ
        {
            public override float Compute(float t)
            {
                if((t *= 2) < 1)
                {
                    return -0.5f * ((float)Math.Sqrt(1 - t * t) - 1);
                }

                return 0.5f * ((float)Math.Sqrt(1 - (t -= 2) * t) + 1);
            }

            public override string ToString()
            {
                return "Circ.InOut";
            }
        }

        public static readonly Circ.CircIn In = new Circ.CircIn();
        public static readonly Circ.CircOut Out = new Circ.CircOut();
        public static readonly Circ.CircInOut InOut = new Circ.CircInOut();
    }
}
