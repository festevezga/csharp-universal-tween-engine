﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Quart : TweenEquation
    {
        public sealed class QuartIn : Quart
        {
            public override float Compute(float t)
            {
                return t * t * t * t;
            }

            public override string ToString()
            {
                return "Quart.In";
            }
        }

        public sealed class QuartOut : Quart
        {
            public override float Compute(float t)
            {
                return -((t -= 1) * t * t * t - 1);
            }

            public override string ToString()
            {
                return "Quart.Out";
            }
        }

        public sealed class QuartInOut : Quart
        {
            public override float Compute(float t)
            {
                if((t *= 2) < 1)
                {
                    return 0.5f * t * t * t * t;
                }

                return -0.5f * ((t -= 2) * t * t * t - 2);
            }

            public override string ToString()
            {
                return "Quart.InOut";
            }
        }

        public static readonly Quart.QuartIn In = new Quart.QuartIn();
        public static readonly Quart.QuartOut Out = new Quart.QuartOut();
        public static readonly Quart.QuartInOut InOut = new Quart.QuartInOut();
    }
}
