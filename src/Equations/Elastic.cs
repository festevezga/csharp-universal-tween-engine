﻿using System;

namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Elastic : TweenEquation
    {
        private const float PI = 3.14159265f;

        public sealed class ElasticIn : Elastic
        {
            public override float Compute(float t)
            {
                float a = _paramA;
                float p = _paramP;
                if(t == 0)
                {
                    return 0;
                }

                if(t == 1)
                {
                    return 1;
                }

                if(!_setP)
                {
                    p = .3f;
                }

                float s;
                if(!_setA || a < 1)
                {
                    a = 1;
                    s = p / 4;
                }
                else
                {
                    s = p / (2 * PI) * (float)Math.Asin(1 / a);
                }

                return -(a * (float)Math.Pow(2, 10 * (t -= 1)) * (float)Math.Sin((t - s) * (2 * PI) / p));
            }

            public override string ToString()
            {
                return "Elastic.In";
            }
        }

        public sealed class ElasticOut : Elastic
        {
            public override float Compute(float t)
            {
                float a = _paramA;
                float p = _paramP;
                
                if(t == 0)
                {
                    return 0;
                }
                
                if(t == 1)
                {
                    return 1;
                }

                if(!_setP)
                {
                    p = .3f;
                }

                float s;
                if(!_setA || a < 1)
                {
                    a = 1;
                    s = p / 4;
                }
                else
                {
                    s = p / (2 * PI) * (float)Math.Asin(1 / a);
                }

                return a * (float)Math.Pow(2, -10 * t) * (float)Math.Sin((t - s) * (2 * PI) / p) + 1;
            }

            public override string ToString()
            {
                return "Elastic.Out";
            }
        }

        public sealed class ElasticInOut : Elastic
        {
            public override float Compute(float t)
            {
                float a = _paramA;
                float p = _paramP;
                
                if(t == 0)
                {
                    return 0;
                }
                
                if((t *= 2) == 2)
                {
                    return 1;
                }
                
                if(!_setP)
                {
                    p = .3f * 1.5f;
                }

                float s;
                if(!_setA || a < 1)
                {
                    a = 1;
                    s = p / 4;
                }
                else
                {
                    s = p / (2 * PI) * (float)Math.Asin(1 / a);
                }

                if(t < 1)
                {
                    return -.5f * (a * (float)Math.Pow(2, 10 * (t -= 1)) * (float)Math.Sin((t - s) * (2 * PI) / p));
                }

                return a * (float)Math.Pow(2, -10 * (t -= 1)) * (float)Math.Sin((t - s) * (2 * PI) / p) * .5f + 1;
            }

            public override string ToString()
            {
                return "Elastic.InOut";
            }
        }

        public static readonly Elastic.ElasticIn In = new Elastic.ElasticIn();
        public static readonly Elastic.ElasticOut Out = new Elastic.ElasticOut();
        public static readonly Elastic.ElasticInOut InOut = new Elastic.ElasticInOut();

        protected float _paramA;
        protected float _paramP;
        protected bool _setA = false;
        protected bool _setP = false;

        public Elastic a(float a)
        {
            _paramA = a;
            _setA = true;
            return this;
        }

        public Elastic p(float p)
        {
            _paramP = p;
            _setP = true;
            return this;
        }
    }
}
