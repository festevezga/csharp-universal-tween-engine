﻿using System;

namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Sine : TweenEquation
    {
        private const float PI = 3.14159265f;

        public sealed class SineIn : Sine
        {
            public override float Compute(float t)
            {
                return (float)-Math.Cos(t * (PI / 2)) + 1;
            }

            public override string ToString()
            {
                return "Sine.In";
            }
        }

        public sealed class SineOut : Sine
        {
            public override float Compute(float t)
            {
                return (float)Math.Sin(t * (PI / 2));
            }

            public override string ToString()
            {
                return "Sine.Out";
            }
        }

        public sealed class SineInOut : Sine
        {
            public override float Compute(float t)
            {
                return -0.5f * ((float)Math.Cos(PI * t) - 1);
            }

            public override string ToString()
            {
                return "Sine.InOut";
            }
        }

        public static readonly Sine.SineIn In = new Sine.SineIn();
        public static readonly Sine.SineOut Out = new Sine.SineOut();
        public static readonly Sine.SineInOut InOut = new Sine.SineInOut();
    }
}
