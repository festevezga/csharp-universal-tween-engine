﻿namespace AurelienRibon.TweenEngine.Equations
{
    public abstract class Bounce : TweenEquation
    {
        public sealed class BounceIn : Bounce
        {
            public override float Compute(float t)
            {
                return 1 - Bounce.Out.Compute(1 - t);
            }

            public override string ToString()
            {
                return "Bounce.In";
            }
        }

        public sealed class BounceOut : Bounce
        {
            public override float Compute(float t)
            {
                if(t < (1 / 2.75))
                {
                    return 7.5625f * t * t;
                }
                else if(t < (2 / 2.75))
                {
                    return 7.5625f * (t -= (1.5f / 2.75f)) * t + .75f;
                }
                else if(t < (2.5 / 2.75))
                {
                    return 7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f;
                }
                else
                {
                    return 7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f;
                }
            }

            public override string ToString()
            {
                return "Bounce.Out";
            }
        }

        public sealed class BounceInOut : Bounce
        {
            public override float Compute(float t)
            {
                if(t < 0.5f)
                {
                    return Bounce.In.Compute(t * 2) * .5f;
                }
                else
                {
                    return Bounce.Out.Compute(t * 2 - 1) * .5f + 0.5f;
                }
            }

            public override string ToString()
            {
                return "Bounce.InOut";
            }
        }

        public static readonly Bounce.BounceIn In = new Bounce.BounceIn();
        public static readonly Bounce.BounceOut Out = new Bounce.BounceOut();
        public static readonly Bounce.BounceInOut InOut = new Bounce.BounceInOut();
    }
}
