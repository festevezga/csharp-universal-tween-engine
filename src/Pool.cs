using System.Collections.Generic;

namespace AurelienRibon.TweenEngine
{
	public abstract class Pool<T> 
	{
        public interface ICallback
        {
            void onPool(T obj);
            void onUnPool(T obj);
        }

		public Pool(int initCapacity, ICallback callback) 
		{
            Objects = new List<T>(initCapacity);
            Callback = callback;
		}

        public void Clear()
        {
            Objects.Clear();
        }

        public void Free(T obj)
        {
            if(!Objects.Contains(obj))
            {
                if(Callback != null)
                {
                    Callback.onPool(obj);
                }
                Objects.Add(obj);
            }
        }

		public T Get() 
		{
			T obj;
			
            if(Objects.Count == 0)
			{
				obj = Create();
			}
			else
			{
				obj = Objects[Objects.Count - 1];
				Objects.RemoveAt(Objects.Count - 1);
			}

            if(Callback != null) 
			{
                Callback.onUnPool(obj);
			}

			return obj;
		}

        protected abstract T Create();

        public int Size
        {
            get
            {
                return Objects.Count;
            }
        }

        private ICallback Callback
        {
            get;
            set;
        }

        private IList<T> Objects
        {
            get;
            set;
        }
	}
}