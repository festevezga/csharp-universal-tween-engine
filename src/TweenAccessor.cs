﻿namespace AurelienRibon.TweenEngine
{
    public abstract class TweenAccessor
    {
        public abstract int GetValues(object target, int tweenType, float[] returnValues);

        public abstract void SetValues(object target, int tweenType, float[] newValues);
    }
}
