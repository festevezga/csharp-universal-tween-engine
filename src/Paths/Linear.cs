﻿using System;

namespace AurelienRibon.TweenEngine.Paths
{
    public class Linear : TweenPath 
    {
        public override float Compute(float t, float[] points, int pointsCnt)
        {
            int segment = (int)Math.Floor((pointsCnt - 1) * t);
            segment = Math.Max(segment, 0);
            segment = Math.Min(segment, pointsCnt - 2);

            t = t * (pointsCnt - 1) - segment;

            return points[segment] + t * (points[segment + 1] - points[segment]);
        }
    }
}
