﻿using System;

namespace AurelienRibon.TweenEngine
{
    public abstract class BaseTween
    {
        private bool _isFinished;

        public virtual BaseTween Build()
        {
            return this;
        }

        public BaseTween Delay(float delayInSeconds)
        {
            DelayInSeconds += delayInSeconds;
            return this;
        }

        public virtual void Free()
        {
        }

        public void Kill()
        {
            IsKilled = true;
        }

        public void Pause()
        {
            IsPaused = true;
        }

        public BaseTween Repeat(int count, float delayInSeconds)
        {
            if(IsStarted)
            {
                throw new TweenException("You can't change the repetitions of a tween or timeline once it is started");
            }

            RepeatCount = count;
            RepeatDelay = delayInSeconds >= 0 ? delayInSeconds : 0;
            IsYoyo = false;
            return this;
        }

        public BaseTween RepeatYoyo(int count, float delayInSeconds)
        {
            if(IsStarted)
            {
                throw new TweenException("You can't change the repetitions of a tween or timeline once it is started");
            }

            RepeatCount = count;
            RepeatDelay = delayInSeconds >= 0 ? delayInSeconds : 0;
            IsYoyo = true;
            return this;
        }

        public void Resume()
        {
            IsPaused = false;
        }

        public BaseTween SetCallback(TweenCallback callback)
        {
            Callback = callback;
            return this;
        }

        public BaseTween SetCallbackTriggers(int flags)
        {
            CallbackTriggers = flags;
            return this;
        }

        public BaseTween SetUserData(object data)
        {
            UserData = data;
            return this;
        }

        public virtual BaseTween Start()
        {
            Build();
            CurrentTime = 0;
            IsStarted = true;
            return this;
        }

        public BaseTween Start(TweenManager manager)
        {
            manager.Add(this);
            return this;
        }

        public void Update(float delta)
        {
            if(!IsStarted || IsPaused || IsKilled)
            {
                return;
            }

            DeltaTime = delta;

            if(!IsInitialized)
            {
                Initialize();
            }

            if(IsInitialized)
            {
                TestRelaunch();
                UpdateStep();
                TestCompletion();
            }

            CurrentTime += DeltaTime;
            DeltaTime = 0;
        }

        protected void CallCallback(int type)
        {
            if(Callback != null && (CallbackTriggers & type) > 0) Callback.onEvent(type, this);
        }

        protected virtual void InitializeOverride()
        {
        }

        protected bool IsReverse(int step)
        {
            return IsYoyo && Math.Abs(step % 4) == 2;
        }

        protected bool IsValid(int step)
        {
            return (step >= 0 && step <= RepeatCount * 2) || RepeatCount < 0;
        }

        private void Initialize()
        {
            if(CurrentTime + DeltaTime >= DelayInSeconds)
            {
                InitializeOverride();
                IsInitialized = true;
                IsIterationStep = true;
                Step = 0;
                DeltaTime -= DelayInSeconds - CurrentTime;
                CurrentTime = 0;
                CallCallback(TweenCallback.Begin);
                CallCallback(TweenCallback.Start);
            }
        }

        private void TestRelaunch()
        {
            if(!IsIterationStep && RepeatCount >= 0 && Step < 0 && CurrentTime + DeltaTime >= 0)
            {
                IsIterationStep = true;
                Step = 0;
                float delta = 0 - CurrentTime;
                DeltaTime -= delta;
                CurrentTime = 0;
                CallCallback(TweenCallback.Begin);
                CallCallback(TweenCallback.Start);
                UpdateOverride(Step, Step - 1, IsIterationStep, delta);

            }
            else if(!IsIterationStep && RepeatCount >= 0 && Step > RepeatCount * 2 && CurrentTime + DeltaTime < 0)
            {
                IsIterationStep = true;
                Step = RepeatCount * 2;
                float delta = 0 - CurrentTime;
                DeltaTime -= delta;
                CurrentTime = Duration;
                CallCallback(TweenCallback.BackBegin);
                CallCallback(TweenCallback.BackStart);
                UpdateOverride(Step, Step + 1, IsIterationStep, delta);
            }
        }

        private void UpdateStep()
        {
            while(IsValid(Step))
            {
                if(!IsIterationStep && CurrentTime + DeltaTime <= 0)
                {
                    IsIterationStep = true;
                    Step -= 1;

                    float delta = 0 - CurrentTime;
                    DeltaTime -= delta;
                    CurrentTime = Duration;

                    if(IsReverse(Step))
                    {
                        ForceStartValues();
                    }
                    else
                    {
                        ForceEndValues();
                    }

                    CallCallback(TweenCallback.BackStart);
                    UpdateOverride(Step, Step + 1, IsIterationStep, delta);

                }
                else if(!IsIterationStep && CurrentTime + DeltaTime >= RepeatDelay)
                {
                    IsIterationStep = true;
                    Step += 1;

                    float delta = RepeatDelay - CurrentTime;
                    DeltaTime -= delta;
                    CurrentTime = 0;

                    if(IsReverse(Step))
                    {
                        ForceEndValues();
                    }
                    else
                    {
                        ForceStartValues();
                    }

                    CallCallback(TweenCallback.Start);
                    UpdateOverride(Step, Step - 1, IsIterationStep, delta);

                }
                else if(IsIterationStep && CurrentTime + DeltaTime < 0)
                {
                    IsIterationStep = false;
                    Step -= 1;

                    float delta = 0 - CurrentTime;
                    DeltaTime -= delta;
                    CurrentTime = 0;

                    UpdateOverride(Step, Step + 1, IsIterationStep, delta);
                    CallCallback(TweenCallback.BackEnd);

                    if(Step < 0 && RepeatCount >= 0)
                    {
                        CallCallback(TweenCallback.BackComplete);
                    }
                    else
                    {
                        CurrentTime = RepeatDelay;
                    }

                }
                else if(IsIterationStep && CurrentTime + DeltaTime > Duration)
                {
                    IsIterationStep = false;
                    Step += 1;

                    float delta = Duration - CurrentTime;
                    DeltaTime -= delta;
                    CurrentTime = Duration;

                    UpdateOverride(Step, Step - 1, IsIterationStep, delta);
                    CallCallback(TweenCallback.End);

                    if(Step > RepeatCount * 2 && RepeatCount >= 0)
                    {
                        CallCallback(TweenCallback.Complete);
                    }

                    CurrentTime = 0;

                }
                else if(IsIterationStep)
                {
                    float delta = DeltaTime;
                    DeltaTime -= delta;
                    CurrentTime += delta;
                    UpdateOverride(Step, Step, IsIterationStep, delta);
                    break;

                }
                else
                {
                    float delta = DeltaTime;
                    DeltaTime -= delta;
                    CurrentTime += delta;
                    break;
                }
            }
        }

        private void TestCompletion()
        {
            IsFinished = RepeatCount >= 0 && (Step > RepeatCount * 2 || Step < 0);
        }

        internal void KillTarget(object target)
        {
            if(ContainsTarget(target))
            {
                Kill();
            }
        }

        internal void KillTarget(object target, int tweenType)
        {
            if(ContainsTarget(target, tweenType))
            {
                Kill();
            }
        }

        protected virtual void Reset()
        {
            Step = -2;
            RepeatCount = 0;
            IsIterationStep = IsYoyo = false;

            DelayInSeconds = Duration = RepeatDelay = CurrentTime = DeltaTime = 0;
            IsStarted = IsInitialized = IsFinished = IsKilled = IsPaused = false;

            Callback = null;
            CallbackTriggers = TweenCallback.Complete;
            UserData = null;

            IsAutoRemoveEnabled = IsAutoStartEnabled = true;
        }

        protected virtual void UpdateOverride(int step, int lastStep, bool isIterationStep, float delta)
        {
        }

        internal abstract bool ContainsTarget(object target);

        internal abstract bool ContainsTarget(object target, int tweenType);
        
        internal abstract void ForceEndValues();

        internal abstract void ForceStartValues();
        
        internal void ForceToEnd(float time)
        {
            CurrentTime = time - FullDuration;
            Step = RepeatCount * 2 + 1;
            IsIterationStep = false;
            
            if(IsReverse(RepeatCount * 2))
            {
                ForceStartValues();
            }
            else
            {
                ForceEndValues();
            }
        }

        internal void ForceToStart()
        {
            CurrentTime = -DelayInSeconds;
            Step = -1;
            IsIterationStep = false;
            
            if(IsReverse(0))
            {
                ForceEndValues();
            }
            else
            {
                ForceStartValues();
            }
        }

        public float CurrentTime
        {
            get;
            internal set;
        }

        public float DelayInSeconds
        {
            get;
            internal set;
        }

        public float Duration
        {
            get;
            internal set;
        }

        public float FullDuration
        {
            get
            {
                if(RepeatCount < 0)
                {
                    return -1;
                }

                return DelayInSeconds + Duration + (RepeatDelay + Duration) * RepeatCount;
            }
        }

        public int RepeatCount
        {
            get;
            internal set;
        }

        public float RepeatDelay
        {
            get;
            internal set;
        }

        public int Step
        {
            get;
            internal set;
        }

        public object UserData
        {
            get;
            internal set;
        }

        public bool IsAutoRemoveEnabled
        {
            get;
            set;
        }

        public bool IsAutoStartEnabled
        {
            get;
            set;
        }

        public bool IsFinished
        {
            get
            {
                return _isFinished || IsKilled;
            }
            internal set
            {
                _isFinished = value;
            }
        }

        public bool IsInitialized
        {
            get;
            internal set;
        }

        public bool IsPaused
        {
            get;
            internal set;
        }

        public bool IsStarted
        {
            get;
            internal set;
        }

        public bool IsYoyo
        {
            get;
            internal set;
        }

        private int CallbackTriggers
        {
            get;
            set;
        }

        private TweenCallback Callback
        {
            get;
            set;
        }

        private float DeltaTime
        {
            get;
            set;
        }

        private bool IsIterationStep
        {
            get;
            set;
        }

        private bool IsKilled
        {
            get;
            set;
        }
    }
}
